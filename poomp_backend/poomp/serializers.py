from rest_framework import serializers
from .models import Poomp, Total_Count, Target

# class TargetSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Target
#         fields = ("target")

class PoompSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poomp
        fields = "__all__"

# class TotalCountSerializer(serializers.HyperlinkedModelSerializer):
#     target = TargetSeralizer()
#     class Meta:
#         model = Total_Count
#         fields = ("count", "target")