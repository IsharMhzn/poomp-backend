from django.shortcuts import render
from django.http import HttpResponse
from .models import Poomp, Total_Count, Target
from .serializers import PoompSerializer
from rest_framework import generics, views

import json

# Create your views here.
class CreatePoompView(generics.CreateAPIView):
    queryset = Poomp.objects.all()
    serializer_class  = PoompSerializer

class CountView(views.APIView):
    def get(self, request, format=None):
        
        counts = Total_Count.objects.all()
        count = 0
        for obj in counts:
            count += obj.count
        
        try:
            target = Target.objects.last()
        except e:
            target = Target.objects.create(target=count + 50, divisor= 150)
            target.save()

        response = {
            "target": target.target,
            "count": count,
        }
        response = json.dumps(response)

        return HttpResponse(response, content_type="application/json")