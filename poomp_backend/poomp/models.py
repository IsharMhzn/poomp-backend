from django.db import models

# Create your models here.
class Poomp(models.Model):
    date = models.DateField(auto_now_add= True)
    time = models.TimeField(auto_now_add= True)
    celebrate = models.CharField(default= "no",max_length=50)

    def save(self, *args, **kwargs):
        ## total count
        count_obj = Total_Count.objects.first()
        if not count_obj:
            count_obj = Total_Count.objects.create(count=0)
        
        count_obj.count += 1
        count_obj.save()

        ## target
        target_obj = Target.objects.last()
        if not target_obj:
            target_obj = Target.objects.create(target=count_obj.count + 50, divisor= 150)
        
        if count_obj.count == target_obj.target:
            new_target = round(((target_obj.target / target_obj.divisor)) * target_obj.target)
            new_divisor = target_obj.divisor * 1.7
            target_obj = Target.objects.create(target=new_target, divisor=new_divisor)
            self.celebrate = "yes"
    
        target_obj.save()

        return super(Poomp, self).save(*args, **kwargs)


class Total_Count(models.Model):
    count = models.BigIntegerField()

class Target(models.Model):
    target = models.BigIntegerField()
    divisor = models.IntegerField()